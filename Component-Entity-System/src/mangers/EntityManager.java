package mangers;
import java.util.HashMap;

import components.EntityComponent;

public class EntityManager {
	private HashMap<Integer, EntityComponent[]> entities;
	private static EntityManager instance;
	
	public static EntityManager get(){
		if(EntityManager.instance == null){
			EntityManager.instance = new EntityManager();
		}
		return EntityManager.instance;
	}
	
	private EntityManager(){
		this.entities = new HashMap<>();
	}
	
	public int length(){
		return entities.size();
	}
	
	public void add(int index, EntityComponent component) {

		EntityComponent[] replace;
		
		if (entities.containsKey(index)) {
			EntityComponent[] temp = entities.get(index);

			replace = new EntityComponent[temp.length + 1];

			for (int i = 0; i < temp.length; i++) {
				replace[i] = temp[i];
			}

			replace[replace.length - 1] = component;
		}else{
			replace = new EntityComponent[1];
			replace[0] = component;
		}
		
		entities.put(index, replace);
	}
	
	/**
	 * Use to retrieve Component of specific component class
	 * @param index relating to abstract entity
	 * @param clazz type of component class to get
	 * @return instance of component implementing EntityComponent, null if none found
	 */
	public EntityComponent get(int index, Class clazz){
		
		if(entities.containsKey(index)){
			for(EntityComponent current: entities.get(index)){
				if(current.getClass().equals(clazz)){
					return current;
				}
			}
		}
		return null;
	}
}
