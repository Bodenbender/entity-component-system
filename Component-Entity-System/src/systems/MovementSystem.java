package systems;

import components.PositionComponent;
import components.VelocityComponent;
import mangers.EntityManager;

public class MovementSystem {
	
	public static void update(){
		EntityManager eManager = EntityManager.get();
		for (int i = 0; i < eManager.length(); i++) {
			PositionComponent pos = (PositionComponent) eManager.get(i, PositionComponent.class);
			VelocityComponent velo = (VelocityComponent) eManager.get(i, VelocityComponent.class);
			if(pos != null && velo != null){
				float newX = pos.getX() + velo.getxVelocity();
				float newY = pos.getY() + velo.getyVelocity();
				
				pos.setX(newX);
				pos.setY(newY);
			}
		}
	}
}
