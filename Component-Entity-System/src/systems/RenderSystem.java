package systems;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import mangers.EntityManager;

import components.ImageComponent;
import components.PositionComponent;

public class RenderSystem {
	
	public static void render(JFrame c){
		Graphics g = c.getGraphics();
		Insets insets = c.getInsets();
		int width = c.getWidth();
		int height = c.getHeight();
		
		BufferedImage backbuffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics bbg = backbuffer.getGraphics(); 
        
        bbg.setColor(Color.WHITE); 
        bbg.fillRect(0, 0, width, height); 
        bbg.setColor(Color.BLACK);
        
		EntityManager eManager = EntityManager.get();
		for (int i = 0; i < eManager.length(); i++) {
			PositionComponent pos = (PositionComponent) eManager.get(i, PositionComponent.class);
			ImageComponent img = (ImageComponent) eManager.get(i, ImageComponent.class);
			if(pos != null && img != null){
				bbg.drawImage(img.getImg(), (int)pos.getX(), (int)pos.getY(), null);
			}
		}
        g.drawImage(backbuffer, insets.left, insets.top, c);
	}
}
