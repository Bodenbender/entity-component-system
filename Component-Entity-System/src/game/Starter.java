package game;
import systems.MovementSystem;
import mangers.EntityManager;
import components.PositionComponent;
import components.VelocityComponent;


public class Starter {

	public static void main(String[] args) {
		
		EntityManager ent = EntityManager.get();
		ent.add(0, new PositionComponent(2, 4));
		ent.add(0, new VelocityComponent(4, 2));
		
		ent.add(1, new PositionComponent(5, 5));
		
		System.out.println(ent.length());
		MovementSystem.update();
		
		PositionComponent test = (PositionComponent) ent.get(0, PositionComponent.class);
		System.out.println(test.getX() + " " + test.getY());

		PositionComponent test2 = (PositionComponent) ent.get(1, PositionComponent.class);
		System.out.println(test2.getX() + " " + test2.getY());
	}

}
