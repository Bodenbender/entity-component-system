package components;

import java.awt.Image;

public class ImageComponent {
	private Image img;
	
	public ImageComponent(Image img){
		this.img = img;
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}
	
}
