package components;

public class VelocityComponent implements EntityComponent{
	private float xVelocity;
	private float yVelocity;
	
	public VelocityComponent(float xVelocity, float yVelocity){
		this.xVelocity = xVelocity;
		this.yVelocity = yVelocity;
	}
	
	public float getxVelocity() {
		return xVelocity;
	}
	public void setxVelocity(float xVelocity) {
		this.xVelocity = xVelocity;
	}
	public float getyVelocity() {
		return yVelocity;
	}
	public void setyVelocity(float yVelocity) {
		this.yVelocity = yVelocity;
	}
}
